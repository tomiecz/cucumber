Feature: Login

  Background:
    When I go to the gmail login page

  Scenario Outline: Successful login
    And I login with "<email>" and "<password>"
    Then I am successfully logged in

    Examples:
      | email | password |
      | miecznikowski.tomasz@gmail.com | tomiecz1988 |
