package mainPage;

import com.opera.core.systems.OperaDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by WebTesting Training on 11/1/2014.
 */
public class MobicaMainPageTest {
    private String baseUrl = "http://www.mobica.com";
    private WebDriver driver;

    //@Before
    public void openBrowser(){
        File fileChromeDriver = new File("C://webautomation/chromedriver.exe");
        File fileIExplorerDriver = new File("C://webautomation/Windows8.1-KB2990999-x86.msi");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
        System.setProperty("webdriver.ie.driver", fileIExplorerDriver.getAbsolutePath());
       // System.setProperty("webdriver.opera.driver", )

        driver = new FirefoxDriver();
        driver.get(baseUrl);
    }

    //@After
    public void closeBrowser(){
        driver.close();
    }

    //@Test
    public void checkMobicaMainPageTitleContains(){
        assertThat(driver.getTitle()).contains("Mobica");
    }

    //@Test
    public void checkMobicaMainPageTitleEquals(){
        String title = driver.getTitle();
        assertThat(driver.getTitle()).isEqualTo("Mobica is a leading provider of software engineering, testing and consultancy services");
    }

}
