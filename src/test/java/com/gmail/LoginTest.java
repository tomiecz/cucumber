package com.gmail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by WebTesting Training on 11/1/2014.
 */
public class LoginTest {
    private WebDriver driver;

    //@Before
    public void openBrowser(){
        File fileChromeDriver = new File("C://webautomation/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());

        driver = new ChromeDriver();
        driver.get("https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/");
    }

    //@After
    public void closeBrowser(){
        driver.close();
    }

    //@Test
    public void loginWithCredentials(){
        String login = "miecznikowski.tomasz@gmail.com";
        String password = "tomiecz1988";

        WebElement emailElement = driver.findElement(By.id("Email"));
        WebElement passwordElement = driver.findElement(By.id("Passwd"));
        WebElement submitBtn = driver.findElement(By.id("signIn"));

        emailElement.sendKeys(login);
        passwordElement.sendKeys(password);

        submitBtn.click();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        assertThat(driver.findElement(By.id("gbqfq")).isDisplayed());
    }
}
