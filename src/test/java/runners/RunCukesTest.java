package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by WebTesting Training on 11/1/2014.
 */

    @RunWith(Cucumber.class)
    @CucumberOptions(
            format = {"pretty","html:target/htmlReports", "json:target/jsonReports/cucumber.json"},
            features= "classpath:testScenarios",
            glue="classpath:stepDefinitions"
            )

    public class RunCukesTest {
    }
