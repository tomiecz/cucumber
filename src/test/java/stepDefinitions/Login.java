package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by WebTesting Training on 11/1/2014.
 */
public class Login {

    public WebDriver driver;

    @When("^I go to the gmail login page$")
    public void I_go_to_the_gmail_login_page(){
        File fileChromeDriver = new File("C://webautomation/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
        driver = new FirefoxDriver();
        driver.get("https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/");
    }

    @And("^I login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void I_login_with_and(String arg1, String arg2){
        String login = arg1;
        String password = arg2;

        WebElement emailElement = driver.findElement(By.id("Email"));
        WebElement passwordElement = driver.findElement(By.id("Passwd"));
        WebElement submitBtn = driver.findElement(By.id("signIn"));

        emailElement.sendKeys(login);
        passwordElement.sendKeys(password);

        submitBtn.click();
    }

    @Then("^I am successfully logged in$")
    public void I_am_successfully_logged_in(){

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        assertThat(driver.findElement(By.id("gbqfq")).isDisplayed());
        driver.close();
    }

}
